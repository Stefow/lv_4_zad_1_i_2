﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv4_zad_1
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
