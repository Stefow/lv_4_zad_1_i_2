﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv4_zad_1
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            int brojRedova = 0;
            int BrojStupaca = 0;
            foreach (List<double> Row in dataset.GetData())
            {
                brojRedova++;
            }
            double[][] matrica=new double [brojRedova][];
            int i=0, j = 0;
            foreach(List<double> Row in dataset.GetData())
            {
                foreach (double el in Row)
                {
                    BrojStupaca++;
                }
                matrica[i] = new double[BrojStupaca];
                foreach (double el in Row)
                {
                    matrica[i][j] = el;
                    Console.Write(matrica[i][j] + " ");//samo radi provjere
                    j++;
                }
                Console.Write("\n");//samo radi provjere
                BrojStupaca = 0;
                j = 0;
                i++;
            }
            return matrica;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }

    }
}
