﻿using System;

namespace Lv4_zad_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\data.csv";
            Dataset dataSet = new Dataset(path);
            Analyzer3rdParty service = new Analyzer3rdParty();
            Adapter adapter = new Adapter(service);
            double[] a= adapter.CalculateAveragePerColumn(dataSet);
            /*
             * zamijenio sam , u ; i . u , da bi radilo sa našom regijom
             0,1; 0,2; 0,3; 0,4; 0,5
             1,1; 1,2; 1,3; 1,4; 1,5
             2,1; 2,2; 2,3; 2,4; 2,5
             */
            foreach (double br in a)
            {
                Console.WriteLine(br);
            }
        }
    }
}
